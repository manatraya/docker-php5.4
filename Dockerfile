FROM debian:wheezy
RUN echo "deb http://packages.dotdeb.org wheezy all" > /etc/apt/sources.list.d/dotdeb.list && \
apt-get update && apt-get install --force-yes -y php5-fpm php5-mysql php5-json php5-mcrypt php5-imagick php5-gd php5-curl php5-apc php5-cli php-pear imagemagick
COPY php-fpm.conf /php-fpm.conf
COPY run.sh /run.sh
CMD ["/run.sh"]
